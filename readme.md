# Overview

This plugin for JIRA is a custom field that allows you to connect to an external database and fetch some values from there.
After adding this custom field to an issue edit screen, you can select one of those values so it will be associated with the current issue.

You can choose how the values are rendered for viewing, editing and searching independently. For the editing, you can use a combobox (single select or cascading select) or an AJAX-style input field.
Internally, the plugin will store only the primary key of the item you have selected, so you can safely edit things in your database, as long as you keep the primary key constant.

# Downloads

Binary releases <= version 4.1 of this JIRA plugin can be downloaded from the [Atlassian Marketplace](https://marketplace.atlassian.com/plugins/org.deblauwe.jira.plugin.database-values-plugin).

Binary releases >= version 4.2 - which are releases of this comdirect bank fork - can be 
downloaded from this repo ([comdirect bank fork releases](https://bitbucket.org/comdirect-bank/jdvp/downloads/)).

# Documentation

Versions <= 4.1: See Wim's repo [wiki](https://bitbucket.org/wimdeblauwe/jdvp/wiki/Home) for the full documentation on installation 
and configuration of the plugin.

Versions >= 4.2: See comdirect bank's [wiki](https://bitbucket.org/comdirectbank/jdvp/wiki/Home) for the full documentation on installation 
and configuration of the plugin.
