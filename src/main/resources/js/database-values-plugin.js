$(function () {

    var IS_LOGGING_ENABLED = false;

    function log(s) {
        IS_LOGGING_ENABLED && AJS.log('DBVP:', s);
    }


    function disableInlineEditForCustomField(customfieldId) {

        if (typeof JIRA.Issues === 'undefined') {
            return;
        }

        var _this = this;
        JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (a, b) {
            _this._disableInlineEditForCustomField(customfieldId);

        });
        this._disableInlineEditForCustomField(customfieldId);
    }

    function _disableInlineEditForCustomField(customfieldId) {
        var view = AJS.$(JIRA.Issues.IssueFieldUtil.getFieldSelector(customfieldId));
        var disableFunction = function (e) {
            if (AJS.$(this).is('.editable-field') || (AJS.$(this).is('.editable-field > *'))) {
                console.log('disabling inline edit for ' + customfieldId);
                view.removeClass('editable-field');
                view.removeClass('inactive');
                view.attr('title', AJS.I18n.getText("at.celix.jira.plugins.zones.fieldDisabled"));
                view.find('.icon').remove();
                view.unbind('mouseenter').unbind('mouseleave').unbind('mousemove').unbind('mouseover');
            }
        };
        if (view.is(':hover')) {
            view.delegate('*,', 'mouseover', disableFunction);
            view.hover(disableFunction);
            view.mousemove(disableFunction);
        } else {
            view.hover(disableFunction);
        }
    }


    function registerAutocomplete() {
        log('registerAutocomplete()');

        $('.dbvp-ui-autocomplete-input').each(function (index, element) {
            var $me = $(this);
            // Note, the element-id is 'databasevalues_<customfieldId>'.
            var customFieldId = $me.data('customfieldid');
            var projectKey = $me.data('projectkey');
            var ajaxPurposeName = $me.data('ajaxpurposename');
            var hiddenInputFieldId = $me.data('hiddeninputfieldid');
            log('register autoCompleteCD, id ' + $me.attr('id')
                + ', customFieldId ' + customFieldId
                + ', projectKey ' + projectKey
                + ', ajaxPurposeName ' + ajaxPurposeName
                + ', hiddenInputFieldId ' + hiddenInputFieldId);

            $(this).autoCompleteCD({
                minChars: 2,
                source: function (
                    searchTerm,        // Aktueller Feld-Inhalt
                    choiceObjects,     // Hier leeres Array. Bei statischen Autocomplete-Daten ist es das Array mit den diesen Daten.
                    suggestFn    // Funktion zur Uebergabe eines Ergebnis-Arrays an Autocomplete
                ) {
                    $.ajax({
                        url: AJS.contextPath() + '/secure/AjaxResultsPageAction!default.jspa',
                        dataType: "json",
                        data: {
                            decorator: "none",
                            q: searchTerm,
                            customfieldId: customFieldId,
                            projectKey: projectKey,
                            purpose: ajaxPurposeName
                        },
                        success: function (data) {
                            log('ajax success, data ' + JSON.stringify(data));

                            var suggestions = $.map(data, function (item) {
                                return {
                                    plainText: item.inputlabel,
                                    formattedText: item.label,
                                    pKey: item.value
                                }
                            });

                            // inputlabel => plainTextChoice
                            // label => formattedChoice
                            // value => pKey

                            log('ajax success, suggestions ');
                            // log('ajax success, suggestions ' + JSON.stringify(suggestions));

                            suggestFn(suggestions);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("ERROR");
                            AJS.log(JSON.stringify(jqXHR));
                            AJS.log(textStatus);
                            AJS.log(errorThrown);
                        }
                    });
                },
                renderChoiceElement: function (choiceObject, searchTerm) {
                    log('renderChoiceElement: choiceObject ' + JSON.stringify(choiceObject) + ', search ' + JSON.stringify(searchTerm));

                    // escape special characters
                    searchTerm = searchTerm.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + searchTerm.split(' ').join('|') + ")", "gi");

                    return '<div class="autocompleteCD-suggestion"'
                        + ' data-val="' + choiceObject.plainText + '"'
                        + ' data-plaintext="' + choiceObject.plainText + '"'
                        + ' data-pkey="' + choiceObject.pKey + '"'
                        + ' >'
                        + choiceObject.plainText.replace(re, "<b>$1</b>") + '</div>';
                },
                onSelect: function (e, $autocompleteElement, valueWithoutSuggestion, $choiceElement) {
                    log('onSelect: $choiceElement plaintext ' + $choiceElement.data('plaintext') + ', pkey ' + $choiceElement.data('pkey'));
                    log('  customFieldId ' + customFieldId + ', hiddenInputFieldId ' + hiddenInputFieldId);
                    $('#databasevalues_' + customFieldId).val($choiceElement.data('plaintext')); // display the selected text
                    $('#' + hiddenInputFieldId).val($choiceElement.data('pkey')); // save selected id to hidden input
                },
                onBlur: function ($autocompleteElement) {
                    var $hiddenInputField = $('#' + hiddenInputFieldId);

                    log('onBlur: '
                        + $autocompleteElement.attr('id') + ' val length ' + $autocompleteElement.val().length
                        + ', '
                        + $hiddenInputField.attr('id') + ' val length ' + $hiddenInputField.val().length);

                    if ($autocompleteElement.val().length === 0 || $hiddenInputField.val().length === 0) {
                        log('  Clear val of ' + $autocompleteElement.attr('id') + ', ' + $hiddenInputField.attr('id'));
                        $autocompleteElement.val('');
                        $hiddenInputField.val('');
                    }
                }
            });
        });
    }

    AJS.toInit(function () {
        log('toInit');

        JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, context, reason) {
            if ($('#edit-issue-dialog').length > 0 || reason === 'inlineEditStarted' || reason === 'dialogReady') {
                log('bind');
                registerAutocomplete();
            }
        });
    });

});
