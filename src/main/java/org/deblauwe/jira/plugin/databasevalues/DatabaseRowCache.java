package org.deblauwe.jira.plugin.databasevalues;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.DatabaseUtil;
import com.atlassian.jira.user.ApplicationUser;
import org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters;
import org.ofbiz.core.entity.ConnectionFactory;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericEntityException;
import org.slf4j.LoggerFactory;

import javax.naming.*;
import javax.servlet.ServletException;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.*;

public class DatabaseRowCache
{
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DatabaseRowCache.class);
	/**
	 * If the configuration does not depend on project keys, then we use this
	 * key in the cache
	 */
	private static final String DEFAULT_PROJECT_KEY = "InternalDefaultProjectKey";
	private static final int KEEP_ALL_PROJECTS_IN_CACHE = -1;

	private final DatabaseValuesCFParameters m_parameters;
	private Map<String, Map<String, DatabaseRow>> m_cache = new HashMap<>();
	private Map<String, Long> m_lastCacheFillTimes = new HashMap<>();

	public DatabaseRowCache(DatabaseValuesCFParameters parameters)
	{
		m_parameters = parameters;
	}

	public Map<String, DatabaseRow> getProjectSpecificCacheMap(String projectKey)
	{
		if (!isDependentOnProjectKey())
		{
			projectKey = DEFAULT_PROJECT_KEY;
		}
		return m_cache.get(projectKey);
	}

	public Collection<DatabaseRow> getDatabaseRows(String projectKey, DatabaseRowCachePurpose purpose)
	{
		Collection<DatabaseRow> collection;
		if (purpose == DatabaseRowCachePurpose.EDIT
				|| (purpose == DatabaseRowCachePurpose.SEARCH && m_parameters.getSqlQuerySearch() == null))
		{
			refreshCacheIfNeeded(projectKey);

			collection = getProjectSpecificCacheMap(projectKey).values();
		}
		else if (purpose == DatabaseRowCachePurpose.SEARCH
				&& m_parameters.getSqlQuerySearch() != null)
		{
			collection = getDatabaseRowsFromDb(projectKey, m_parameters.getSqlQuerySearch());
		}
		else
		{
			throw new IllegalArgumentException("Unsupported purpose!");
		}
		// Make sure the returned list of database rows is ordered like the user wanted (specified by using ORDER BY)
		List<DatabaseRow> list = new ArrayList<>(collection.size());
		list.addAll(collection);
		Collections.sort(list);

		return list;
	}

	/**
	 * @param projectKey the JIRA project key. Can be null.
	 */
	public void refreshCacheIfNeeded(String projectKey)
	{
		if (!isDependentOnProjectKey())
		{
			projectKey = DEFAULT_PROJECT_KEY;
		}

		if (System.currentTimeMillis() - getLastCacheFillTime(projectKey) > m_parameters.getCacheTimeout())
		{
			Map<String, DatabaseRow> projectCache = getProjectSpecificCacheMap(projectKey);
			if (projectCache == null)
			{
				logger.debug("No project cache for " + projectKey);
				// First check if some project must be removed from the cache
				if (m_parameters.getNumberOfProjectsInCache() != KEEP_ALL_PROJECTS_IN_CACHE)
				{
					if (m_cache.size() == m_parameters.getNumberOfProjectsInCache())
					{
						String oldCacheProjectKey = getOldestCacheProjectKey();
						if (oldCacheProjectKey != null)
						{
							logger.debug("Removing " + oldCacheProjectKey + " project cache");
							m_cache.remove(oldCacheProjectKey);
							m_lastCacheFillTimes.remove(oldCacheProjectKey);
						}
						else
						{
							logger.error("Could not remove a project map from the cache, although there are already " + m_cache.size() + " projects in the cache (max projects=" + m_parameters.getNumberOfProjectsInCache() + ")");
						}
					}
				}

				projectCache = new HashMap<>();

				// Add the new projects map to the cache
				m_cache.put(projectKey, projectCache);
			}
			else
			{
				projectCache.clear();
			}

			List<DatabaseRow> databaseRows = getDatabaseRowsFromDb(projectKey);
			for (DatabaseRow databaseRow : databaseRows)
			{
				Object key = databaseRow.getValue(m_parameters.getPrimaryKeyColumnNumber());
				if (key != null)
				{
					projectCache.put(key.toString(), databaseRow);
				}
				else
				{
					logger.error("Are you sure the property 'primarykey.column.number' is set correctly?");
					logger.error("primarykey.column.number=" + m_parameters.getPrimaryKeyColumnNumber());
					logger.error("databaseRow = " + databaseRow);
				}
			}

			m_lastCacheFillTimes.put(projectKey, new Long(System.currentTimeMillis()));
		}
	}

	/**
	 * Method for getting a certain record from the database directly (without using the cache).
	 *
	 * @param id primary key of the record
	 * @return the record
	 */
	public DatabaseRow getDatabaseRowFromDatabase(String id)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			conn = createConnection();

			stmt = conn.createStatement();

			String sqlQuery = m_parameters.doSqlSubstitutions();
			if (isDependentOnProjectKey())
			{
				if (sqlQuery.indexOf("like") == -1)
				{
					logger.error("You need to use like in your WHERE clause to use ${jira.project.key}");
				}
				sqlQuery = sqlQuery.replaceAll("\\$\\{jira.project.key\\}", "%");
				sqlQuery += " AND " + m_parameters.getPrimaryKeyColumnName() + "=" + id;
				logger.error(sqlQuery);
			}
			else
			{
				throw new IllegalArgumentException("This function should only be used when depending on a project key!");
			}

			sqlQuery = replaceJiraUserIfPresent(sqlQuery);

			rs = stmt.executeQuery(sqlQuery);
			ResultSetMetaData data = rs.getMetaData();

			DatabaseRow result;
			if (rs.next())
			{
				result = new DatabaseRow();
				for (int i = 1; i < data.getColumnCount() + 1; i++)
				{
					// Work 0-based in the cache
					result.addDatabaseColumn(i - 1, rs.getObject(i));
				}
			}
			else
			{
				throw new IllegalArgumentException("Could not find entry with id " + id + " in the database. Query: " + sqlQuery);
			}

			return result;
		}
		catch (SQLException | GenericEntityException | NamingException e)
		{
			logger.error(e.getMessage(), e);
			return null;
		}
		finally
		{
			DatabaseUtil.closeQuietly(stmt);
			DatabaseUtil.closeQuietly(conn);
			DatabaseUtil.closeQuietly(rs);
		}
	}

	public Collection<DatabaseRow> getAllDatabaseRows(DatabaseRowCachePurpose purpose)
	{
		Collection<DatabaseRow> result;
		if (!isDependentOnProjectKey())
		{
			result = getDatabaseRows(DEFAULT_PROJECT_KEY, purpose);
		}
		else
		{
			result = getAllDatabaseRowsForProjectDependConfig();
		}
		return result;
	}

	private Collection<DatabaseRow> getAllDatabaseRowsForProjectDependConfig()
	{
		// Go directly to the database, we don't want to fill up our cache with the complete database!
		return getDatabaseRowsFromDb(null);
	}

	/**
	 * Returns the database rows that match the project key. If it is null, then
	 * all rows are returned.
	 *
	 * @param projectKey the project key. Can be null.
	 * @return the matching rows
	 */
	private List<DatabaseRow> getDatabaseRowsFromDb(String projectKey)
	{
		return getDatabaseRowsFromDb(projectKey, m_parameters.doSqlSubstitutions());
	}

	private List<DatabaseRow> getDatabaseRowsFromDb(String projectKey, String sqlQuery)
	{
		sqlQuery = replaceProjectKeyIfPresent(projectKey, sqlQuery);
		sqlQuery = replaceJiraUserIfPresent(sqlQuery);

		return queryDatabase(sqlQuery);
	}

	private List<DatabaseRow> queryDatabase(String sqlQuery)
	{
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			conn = createConnection();
			stmt = conn.createStatement();

			logger.debug("SQL query: " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			ResultSetMetaData data = rs.getMetaData();

			List<DatabaseRow> result = new LinkedList<>();
			long rowNumber = 0;
			while (rs.next())
			{
				DatabaseRow row = new DatabaseRow();
				row.setRowNumber(rowNumber);
				for (int i = 1; i < data.getColumnCount() + 1; i++)
				{
					// Work 0-based in the cache
					row.addDatabaseColumn(i - 1, rs.getObject(i));
				}

				result.add(row);
				rowNumber++;
			}

			if (logger.isDebugEnabled())
			{
				logger.debug("Query returned " + result.size() + " rows");
				List<DatabaseRow> firstFiveMax = result.subList(0, Math.min(result.size(), 5));
				logger.debug("Showing first " + firstFiveMax.size() + " records for debugging:");
				for (DatabaseRow databaseRow : firstFiveMax)
				{
					logger.debug(databaseRow.toString());
				}
			}

			return result;
		}
		catch (SQLException | NamingException | GenericEntityException e)
		{
			logger.error(e.getMessage(), e);
			return new ArrayList<>();
		}
		finally
		{
			DatabaseUtil.closeQuietly(stmt);
			DatabaseUtil.closeQuietly(conn);
			DatabaseUtil.closeQuietly(rs);
		}
	}

	private String replaceProjectKeyIfPresent(String projectKey, String sqlQuery)
	{
		if (sqlQuery.indexOf("${jira.project.key}") != -1)
		{
			if (projectKey != null)
			{
				sqlQuery = sqlQuery.replaceAll("\\$\\{jira.project.key\\}", projectKey);
			}
			else
			{
				if (sqlQuery.indexOf("like") == -1)
				{
					logger.error("You need to use like in your WHERE clause to use ${jira.project.key}");
				}
				sqlQuery = sqlQuery.replaceAll("\\$\\{jira.project.key\\}", "%");
			}
		}
		return sqlQuery;
	}

	private String replaceJiraUserIfPresent(String sqlQuery)
	{
		if (sqlQuery.indexOf("${jira.user}") != -1)
		{
			ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
			String userName = user.getName();
			logger.debug("Replacing ${jira.user} with " + userName);
			sqlQuery = sqlQuery.replaceAll("\\$\\{jira.user\\}", userName);
		}
		return sqlQuery;
	}

	private String getOldestCacheProjectKey()
	{
		long oldestTime = Long.MAX_VALUE;
		String result = null;
		for (String projectKey : m_lastCacheFillTimes.keySet())
		{
			Long fillTime = m_lastCacheFillTimes.get(projectKey);
			if (fillTime.longValue() < oldestTime)
			{
				result = projectKey;
				oldestTime = fillTime.longValue();
			}
		}
		return result;
	}

	private long getLastCacheFillTime(String projectKey)
	{
		long result;
		Long fillTime = m_lastCacheFillTimes.get(projectKey);
		if (fillTime != null)
		{
			result = fillTime.longValue();
		}
		else
		{
			result = 0;
		}
		return result;
	}

	private boolean isDependentOnProjectKey()
	{
		String sqlQuery = m_parameters.doSqlSubstitutions();
		return sqlQuery.indexOf("${jira.project.key}") != -1;
	}

	private Connection createConnection() throws NamingException, SQLException, GenericEntityException {

		Connection connection;
		if( m_parameters.isUseInternalJiraDatabase() )
		{
			DelegatorInterface delegator = ComponentAccessor.getComponent(DelegatorInterface.class);
			String helperName = delegator.getGroupHelperName("default");
			logger.info("Connecting with internal jira db registered as " + helperName);
			connection = ConnectionFactory.getConnection( helperName );
		}else {

			InitialContext initialContext = new InitialContext();
			Context environmentContext = (Context) initialContext.lookup("java:comp/env");
			DataSource dataSource = (DataSource) environmentContext.lookup(m_parameters.getDatabaseDataSourceName());
			logger.info("Connecting with data source " + m_parameters.getDatabaseDataSourceName());
			connection = dataSource.getConnection();
		}

		return connection;
	}

	void addDemoData()
	{
		Statement stmt = null;
		Connection conn = null;
		try
		{
			conn = createConnection();

			stmt = conn.createStatement();

			stmt.executeUpdate("DROP TABLE customer IF EXISTS");
			stmt.executeUpdate("CREATE TABLE customer ( id INTEGER IDENTITY, firstname VARCHAR(256), lastname VARCHAR(256), city VARCHAR(256), country VARCHAR(256), projectkey VARCHAR(256) )");
			stmt.executeUpdate("INSERT INTO customer VALUES( 1, 'Wim', 'Deblauwe', 'Heule', 'Belgium', 'TST' )");
			stmt.executeUpdate("INSERT INTO customer VALUES( 2, 'Victor', 'Deblauwe', 'Heule', 'Belgium', 'TST' )");
			stmt.executeUpdate("INSERT INTO customer VALUES( 3, 'Jules', 'Deblauwe', 'Heule', 'Belgium', 'PJT' )");
			stmt.executeUpdate("INSERT INTO customer VALUES( 4, 'John', 'Atlassian', 'Sidney', 'Australia', 'PJT')");
			stmt.executeUpdate("INSERT INTO customer VALUES( 5, 'Steve', 'Urkel', 'Copenhagen', 'Denmark', 'TST')");
			stmt.executeUpdate("INSERT INTO customer VALUES( 6, 'Tim', 'O''hara', 'Brussel', 'Belgium', 'PJT')");
			stmt.executeUpdate("INSERT INTO customer VALUES( 7, 'Ervin', '\"Magic\" Johnson', 'Sidney', 'Australia', 'PJT')");
			stmt.executeUpdate("INSERT INTO customer VALUES( 8, 'admin', 'Last', 'Brisbane', 'Australia', 'PJT')");
		}
		catch (SQLException e)
		{
			logger.error(e.getMessage(), e);
		} catch (NamingException | GenericEntityException e)
		{
			e.printStackTrace();
		} finally
		{
			DatabaseUtil.closeQuietly(stmt);
			DatabaseUtil.closeQuietly(conn);
		}
	}

	public List<DatabaseRow> getQueryValues(String query)
	{
		return queryDatabase(query);
	}
}
