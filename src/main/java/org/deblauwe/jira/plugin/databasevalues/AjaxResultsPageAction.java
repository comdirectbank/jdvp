package org.deblauwe.jira.plugin.databasevalues;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.request.RequestMethod;
import com.atlassian.jira.security.request.SupportedMethods;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import org.apache.log4j.Logger;
import org.deblauwe.jira.plugin.databasevalues.ajax.AjaxViewResultsPurpose;
import webwork.action.ServletActionContext;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Wim Deblauwe
 */
@SupportedMethods({RequestMethod.GET})
public class AjaxResultsPageAction extends JiraWebActionSupport
{
	private static final Logger logger = Logger.getLogger( AjaxResultsPageAction.class );

	/**
	 * 
	 */
	private static final long serialVersionUID = 7990817576683683217L;

	/**
	 * Returns the HTML for the AJAX div with the choices for user,
	 * depending on what already has been typed.
	 *
	 * @param i18nBean helper bean for internationalization
	 * @return a HTML snippet
	 */
	public String getHtml( I18nHelper i18nBean )
	{
		ServletActionContext.getResponse().setContentType( "application/json" );

		String customfieldId = request.getParameter( "customfieldId" );
		if (customfieldId != null)
		{
			DatabaseValuesViewHelper valuesViewHelper = getDatabaseValuesViewHelper( customfieldId );
			String query = request.getParameter( "q" );
			String projectKey = request.getParameter( "projectKey" );
            String purpose = request.getParameter( "purpose" );

			logger.debug("Finding parameter for field: " + customfieldId + "=>" + request.getParameterNames().toString());
            Enumeration paramNames = request.getParameterNames();
			Map<String, String> substitutions = new HashMap<String, String>();

			while(paramNames.hasMoreElements())
			{
    			String paramName = (String)paramNames.nextElement();
	        	String[] paramValues = request.getParameterValues(paramName);

				// save all parameters in a map for later sql substitution
				if (paramName != null && paramValues != null)
				{
    				substitutions.put(paramName, paramValues[0]);
				}
			}

			valuesViewHelper.setSqlSubstitutions(substitutions);

			return valuesViewHelper.getHtmlForAjaxResults( query, projectKey, i18nBean, AjaxViewResultsPurpose.valueOf(purpose) );
		}
		else
		{
			return "Please enter a customfieldId!";
		}
	}

	private DatabaseValuesViewHelper getDatabaseValuesViewHelper( String customfieldId )
	{
		CustomField customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject( customfieldId );
		Map map = customField.getCustomFieldType().getVelocityParameters( null, customField, null );
		return (DatabaseValuesViewHelper)map.get( "databaseValuesViewHelper" );
	}
}
