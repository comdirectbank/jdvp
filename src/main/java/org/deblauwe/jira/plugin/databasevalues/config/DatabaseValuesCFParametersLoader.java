package org.deblauwe.jira.plugin.databasevalues.config;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.issue.fields.CustomField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Wim Deblauwe
 */
public class DatabaseValuesCFParametersLoader
{
	private static final Logger logger = LoggerFactory.getLogger(DatabaseValuesCFParametersLoader.class);

	/**
	 * Cache is refreshed every 15 minutes by default
	 */
	static final long DEFAULT_CACHE_TIMEOUT = 15 * 60 * 1000;
	private static final int DEFAULT_NUMBER_OF_PROJECTS_IN_CACHE = 1;
	private static final int DEFAULT_EDITPATTERN_WIDTH = 50;
	private static final int DEFAULT_SEARCHPATTERN_WIDTH = 50;

	private DatabaseValuesCFParametersLoader()
	{
	}

	/**
	 * Loads the parameters for the {@link org.deblauwe.jira.plugin.databasevalues.DatabaseValuesCFType}
	 * from a properties file from disk.
	 *
	 * @param propertiesFilename the file name of the customfield's properties file
	 * @return a Properties object containing the parameters for the given custom field.
	 *         Null if the properties file could not be loaded.
	 */
	private static Properties findAndLoadPropertiesFile(String propertiesFilename )
	{
		InputStream stream;
		Properties properties = new Properties();

		try {
			logger.debug("Looking for properties-file {} with getResourceAsStream()", propertiesFilename);
			stream = ClassLoaderUtils.getResourceAsStream( propertiesFilename, DatabaseValuesCFParametersLoader.class );
			if (stream != null) {
				logger.debug("  Found");
				properties.load( stream );
				return properties;
			}

			logger.debug("Looking for properties-file {} with getResource()", propertiesFilename);
			URL resource = ClassLoaderUtils.getResource( propertiesFilename, DatabaseValuesCFParametersLoader.class );
			if( resource != null )
			{
				logger.debug("  Found");
				File propFile = new File( resource.getFile() );
				stream = new FileInputStream( propFile );
				properties.load( stream );
				return properties;
			}

			JiraHome jiraHome = ComponentAccessor.getComponentOfType(JiraHome.class);
			logger.debug("Looking for properties-file {} in Jira home {}", propertiesFilename, jiraHome.getHomePath());
			File propertiesFile = new File(jiraHome.getHomePath() + "/" + propertiesFilename);
			if (propertiesFile.exists()) {
				logger.debug("  Found");
				stream = new FileInputStream(propertiesFile.getAbsoluteFile());
				properties.load( stream );
				return properties;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Loads the parameters for the {@link org.deblauwe.jira.plugin.databasevalues.DatabaseValuesCFType}
	 * from a properties file from disk.
	 *
	 * @param customField the customfield to load the properties of
	 * @return a {@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters} object.
	 *         Null if the properties could not be loaded.
	 */
	public static DatabaseValuesCFParameters loadParameters( CustomField customField )
	{
		DatabaseValuesCFParameters result;
		String propertiesFilename = "jira-database-values-plugin-" + customField.getIdAsLong() + ".properties";

		Properties properties = findAndLoadPropertiesFile(propertiesFilename);
		if(properties == null)
		{
			logger.error("Could not load file {}", propertiesFilename);
			return null;
		}

		result = new DatabaseValuesCFParameters();
		if (properties.containsKey("database.datasource.name"))
		{
			result.setDatabaseDataSourceName(properties.getProperty("database.datasource.name").trim());
		}
		else
		{
			result.setUseInternalJiraDatabase(true);
		}

		result.setSqlQuery( properties.getProperty( "sql.query" ).trim() );
		if( properties.getProperty( "sql.query.search" ) != null )
		{
			result.setSqlQuerySearch( properties.getProperty( "sql.query.search" ) );
		}
		else
		{
			result.setSqlQuerySearch( null );
		}
		result.setPrimaryKeyColumnNumber( Integer.parseInt( properties.getProperty( "primarykey.column.number" ).trim() ) );
		result.setRenderingViewPattern( properties.getProperty( "rendering.viewpattern" ).trim() );
		result.setRenderingEditPattern( properties.getProperty( "rendering.editpattern" ).trim() );
		if (properties.getProperty( "rendering.editwidth" ) != null)
		{
			result.setRenderingEditWidth( Integer.parseInt( properties.getProperty( "rendering.editwidth" ).trim() ) );
		}
		else
		{
			result.setRenderingEditWidth( DEFAULT_EDITPATTERN_WIDTH );
		}
		if (properties.getProperty( "rendering.searchwidth" ) != null)
		{
			result.setRenderingSearchWidth( Integer.parseInt( properties.getProperty( "rendering.searchwidth" ).trim() ) );
		}
		else
		{
			result.setRenderingSearchWidth( DEFAULT_SEARCHPATTERN_WIDTH );
		}
		result.setRenderingSearchPattern( properties.getProperty( "rendering.searchpattern" ).trim() );
		String changeLogViewPattern = properties.getProperty("rendering.changelog.viewpattern");
		if( changeLogViewPattern != null )
		{
			result.setChangeLogViewPattern( changeLogViewPattern.trim() );
		}
		else
		{
			result.setChangeLogViewPattern( result.getRenderingViewPattern() );
		}
		String statisticsViewPattern = properties.getProperty("rendering.statistics.viewpattern");
		if( statisticsViewPattern != null )
		{
			result.setStatisticsViewPattern( statisticsViewPattern.trim() );
		}
		else
		{
			result.setStatisticsViewPattern( result.getRenderingViewPattern() );
		}
		if (properties.getProperty( "cache.timeout" ) != null)
		{
			result.setCacheTimeout( Long.parseLong( properties.getProperty( "cache.timeout" ).trim() ) );
		}
		else
		{
			result.setCacheTimeout( DEFAULT_CACHE_TIMEOUT );
		}
		if (properties.getProperty( "cache.maximum.projects" ) != null)
		{
			result.setNumberOfProjectsInCache( Integer.parseInt( properties.getProperty( "cache.maximum.projects" ).trim() ) );
		}
		else
		{
			result.setNumberOfProjectsInCache( DEFAULT_NUMBER_OF_PROJECTS_IN_CACHE );
		}
		// This property is needed for sorting when you depend on the project key
		if (properties.getProperty( "primarykey.column.name" ) != null)
		{
			result.setPrimaryKeyColumnName( properties.getProperty( "primarykey.column.name" ).trim() );
		}

		if (properties.getProperty( "edit.type" ) != null)
		{
			if (Integer.parseInt( properties.getProperty( "edit.type" ).trim() ) == DatabaseValuesCFParameters.EDIT_TYPE_AJAX_INPUT)
			{
				result.setEditType( DatabaseValuesCFParameters.EDIT_TYPE_AJAX_INPUT );
			}
			else if (Integer.parseInt( properties.getProperty( "edit.type" ).trim() ) == DatabaseValuesCFParameters.EDIT_TYPE_CASCADING_SELECT)
			{
				result.setEditType( DatabaseValuesCFParameters.EDIT_TYPE_CASCADING_SELECT );
				result.setGroupingColumnNumber( Integer.parseInt( properties.getProperty( "rendering.editpattern.group.column.number" ).trim() ) );
			}
			else
			{
				result.setEditType( DatabaseValuesCFParameters.EDIT_TYPE_COMBOBOX );
			}
		}
		else
		{
			result.setEditType( DatabaseValuesCFParameters.EDIT_TYPE_COMBOBOX );
		}

		if( properties.getProperty( "search.type") != null )
		{
			if( Integer.parseInt( properties.getProperty( "search.type" ).trim() ) == DatabaseValuesCFParameters.SEARCH_TYPE_AJAX_INPUT)
			{
				result.setSearchType( DatabaseValuesCFParameters.SEARCH_TYPE_AJAX_INPUT );
			}
			else
			{
				result.setSearchType( DatabaseValuesCFParameters.SEARCH_TYPE_LIST );
			}
		}
		else
		{
			result.setSearchType( DatabaseValuesCFParameters.SEARCH_TYPE_LIST );
		}

		if (properties.getProperty( "rendering.sortpattern" ) != null)
		{
			result.setSortingViewPattern( properties.getProperty( "rendering.sortpattern" ).trim() );
		}
		else
		{
			result.setSortingViewPattern( result.getRenderingViewPattern() );
		}

		if( properties.getProperty( "sql.linkedfields") != null )
		{
			logger.warn( "Linked fields are no longer supported!" );
		}

		result.setJqlQueries( loadJqlQueries(properties) );

		return result;

	}

	private static Map<String, String> loadJqlQueries(Properties properties)
	{
		int i = 1;

		Map<String, String> jqlQueries = new HashMap<>();
		String queryReferencePropertyKey = "jql." + i + ".query.reference";
		String queryReferenceProperty = properties.getProperty(queryReferencePropertyKey);
		while( queryReferenceProperty != null )
		{
			String queryPropertyKey = "jql." + i + ".query";
			String queryProperty = properties.getProperty(queryPropertyKey);
			if( queryProperty != null )
			{
				jqlQueries.put( queryReferenceProperty, queryProperty );
			}
			else
			{
				logger.warn( "Found '" + queryReferencePropertyKey + "' but no matching query (Was looking for '" + queryPropertyKey +"')" );
			}
			i++;

			queryReferencePropertyKey = "jql." + i + ".query.reference";
			queryReferenceProperty = properties.getProperty(queryReferencePropertyKey);
		}

		return jqlQueries;
	}
}
