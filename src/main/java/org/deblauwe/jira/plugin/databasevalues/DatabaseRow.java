package org.deblauwe.jira.plugin.databasevalues;

import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Wim Deblauwe
 */
public class DatabaseRow implements Comparable
{
// ------------------------------ FIELDS ------------------------------

	private long m_rowNumber;

	private Map<Integer, Object> m_numberToValues = new TreeMap<>();

// --------------------------- CONSTRUCTORS ---------------------------

	public DatabaseRow()
	{
	}

// -------------------------- PUBLIC METHODS --------------------------

	public long getRowNumber()
	{
		return m_rowNumber;
	}

	public void setRowNumber( long rowNumber )
	{
		m_rowNumber = rowNumber;
	}

	public void addDatabaseColumn( int columnNumber, Object value )
	{
		m_numberToValues.put( new Integer( columnNumber ), value );
	}

	public Object getValue( int columnNumber )
	{
		return m_numberToValues.get( new Integer( columnNumber ) );
	}

	public int getNumberOfValues()
	{
		return m_numberToValues.keySet().size();
	}

	public String toString()
	{
		return "DatabaseRow[nr="+ m_rowNumber + ", values=" + m_numberToValues + "]";
	}

// ------------------------ INTERFACE METHODS ------------------------

// --------------------- Interface Comparable ---------------------

	@Override
	public int compareTo( @NotNull Object o )
	{
		int result;
		if (o instanceof DatabaseRow)
		{
			DatabaseRow otherRow = (DatabaseRow)o;
			long rowNumber = this.getRowNumber();
			long otherRowNumber = otherRow.getRowNumber();
			result = (rowNumber < otherRowNumber ? -1 : (rowNumber == otherRowNumber ? 0 : 1));
		}
		else
		{
			result = 0;
		}
		return result;
	}
}
